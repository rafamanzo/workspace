## Useful commands

Update all repositories:

```bash
git submodule update --recursive --remote
```

Clone all repositories which are not cloned yet:

```bash
git submodule update --init --recursive
```
